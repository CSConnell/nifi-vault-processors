NiFi Vault Processors
==========================

Background
----------

NiFi Vault Processors allow NiFi users to interact with secrets in Vault.  Ultimately,
interacting includes not only reading secrets from Vault, but also adding or removing them.

Processors
----------

### ConsumeVault

#### Features


This particular processor will:

* Use an initial access token to fetch the role id and a secret id, to make further requests
* Use that secret to fetch a token, which is stored into the processor cluster state
* Retrieve the lease duration for the token and the number of uses, and keep that info in the cluster state
* Store the token in the processor cluster state for as long as it is valid
* Utilize the token to request secrets and pass them on appropriately
* Ensure that the token has not expired and/or make sure there are still token uses available
* Make new token requests if needed.
* Pass any failures on to the failure path.


##### Requirements

You must have a Vault server set up that is reachable by NiFi.  Additionally, you will need:

* A root token (not necessarily **the** root token) that NiFi uses to look up the role id and role secret
* An AppRole needs to be configured in Vault, and that AppRole should have access to your secrets
* Some secrets!

##### Usage

* Place the ConsumeVault Processor on the data flow canvas
* Configure the:
    * App Role Name
    * Initial Access Token
    * The Vault Server address and port
    * The SSL Context Service (Optional, but recommended)
    * The secret you want to look up, which can be an attribute (like "${secret"} or configured directly (like "secret/hello"))
    * The keys you want to fetch from the secret as a comma separated list (Optional)
    * The destination - flowfile attributes or flowfile content.

Defaults are configured, with the exception of the Initial Access Token.

If you don't select specific keys to fetch, then all will be fetched.  Entering a key that doesn't
exist is fine, it will be ignored.

**However** if you select a secret that does not exist, Vault will return a 403.  Vault will always
return a 403 if your token isn't correct, a secret does not exist, or pretty much anything else.  That
is intentional, as Vault does not want to give bad actors the ability to try and figure out
what secrets exist or do not exist.
