
package com.staq.nifi.processors.vaultprocessors;

import com.staq.nifi.processors.vaultprocessors.util.ClientToken;
import org.apache.nifi.components.PropertyDescriptor;
import org.apache.nifi.flowfile.FlowFile;
import org.apache.nifi.annotation.behavior.ReadsAttribute;
import org.apache.nifi.annotation.behavior.ReadsAttributes;
import org.apache.nifi.annotation.behavior.WritesAttribute;
import org.apache.nifi.annotation.behavior.WritesAttributes;
import org.apache.nifi.annotation.lifecycle.OnScheduled;
import org.apache.nifi.annotation.documentation.CapabilityDescription;
import org.apache.nifi.annotation.documentation.SeeAlso;
import org.apache.nifi.annotation.documentation.Tags;
import org.apache.nifi.processor.exception.ProcessException;
import org.apache.nifi.processor.AbstractProcessor;
import org.apache.nifi.processor.ProcessContext;
import org.apache.nifi.processor.ProcessSession;
import org.apache.nifi.processor.ProcessorInitializationContext;
import org.apache.nifi.processor.Relationship;
import org.apache.nifi.processor.util.StandardValidators;
import org.apache.nifi.expression.AttributeExpression;

import org.apache.nifi.logging.ComponentLog;


// We need this for storing state for the processor
import org.apache.nifi.components.state.Scope;
import org.apache.nifi.components.state.StateManager;
import org.apache.nifi.components.state.StateMap;

import java.io.IOException;
import java.util.*;

import java.time.*;
import java.time.format.DateTimeFormatter;

// import the Vault Library
import com.bettercloud.vault.VaultConfig;
import com.bettercloud.vault.SslConfig;
import com.bettercloud.vault.Vault;
import com.bettercloud.vault.VaultException;
import com.bettercloud.vault.response.AuthResponse;

// For writing out to the flowFile Content
import java.io.OutputStream;

import org.apache.nifi.processor.io.OutputStreamCallback;

// For returning JSON
import com.google.gson.Gson;

// For SSL
import org.apache.nifi.ssl.SSLContextService;
import org.apache.nifi.components.ValidationContext;
import org.apache.nifi.components.ValidationResult;


@SuppressWarnings({"WeakerAccess", "SpellCheckingInspection"})
@Tags({"vault"})
@CapabilityDescription("Provide a description")
@SeeAlso({})
@ReadsAttributes({@ReadsAttribute(attribute = "secret", description = "The vault secret to lookup.")})
@WritesAttributes({@WritesAttribute(attribute = "Dynamic", description = "The written attributes depend upon what is contained in the Vault secret.")})
public class ConsumeVault extends AbstractProcessor {

    // Add the properties
    @SuppressWarnings("WeakerAccess")
    public static final PropertyDescriptor PROP_ROLE_NAME = new PropertyDescriptor.Builder()
            .name("App Role Name")
            .description("The App Role Name the processor should use to access the secret store.")
            .required(true)
            .addValidator(StandardValidators.createAttributeExpressionLanguageValidator(AttributeExpression.ResultType.STRING, true))
            .defaultValue("NiFiRole")
            .expressionLanguageSupported(true)
            .build();

    public static final PropertyDescriptor PROP_INITIAL_TOKEN = new PropertyDescriptor.Builder()
            .name("Initial Access Token")
            .description("The initial token required to retrieve the token for the AppRole.")
            .required(true)
            .addValidator(StandardValidators.createAttributeExpressionLanguageValidator(AttributeExpression.ResultType.STRING, true))
            .expressionLanguageSupported(true)
            .sensitive( true ) //Added for JIRA PLAT-156 to make this property sensitive.
            .build();


    public static final PropertyDescriptor PROP_VAULT_SERVER = new PropertyDescriptor.Builder()
            .name("The Vault server address and port")
            .description("The location and address of the server that should be used to look up secrets.")
            .required(true)
            .addValidator(StandardValidators.createAttributeExpressionLanguageValidator(AttributeExpression.ResultType.STRING, true))
            .defaultValue("https://localhost:8200")
            .build();

    public static final PropertyDescriptor PROP_SECRET_LOOKUP = new PropertyDescriptor.Builder()
            .name("The secret to request from Vault.")
            .description("The secret to request from Vault.")
            .required(true)
            .addValidator(StandardValidators.createAttributeExpressionLanguageValidator(AttributeExpression.ResultType.STRING, true))
            .defaultValue("${secret}")
            .expressionLanguageSupported(true)
            .build();

    // We need to support SSL/TLS
    public static final PropertyDescriptor PROP_SSL_CONTEXT_SERVICE = new PropertyDescriptor.Builder()
            .name("SSL Context Service")
            .description("The SSL Context Service used to provide client certificate information for TLS/SSL (https) connections.")
            .required(false)
            .identifiesControllerService(SSLContextService.class)
            .build();

    // Allows us to define specific key/value pairs we want to get from the secret.
    public static final PropertyDescriptor PROP_KEYS_TO_FETCH = new PropertyDescriptor.Builder()
            .name("Keys to Fetch")
            .description("A comma separated list of keys whose values should be retrieved from the secret store.  If not specified, all keys will be retrieved.")
            .required(false)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .build();

    // Define the attributes that are appropriate for destinations
    public static final String DESTINATION_ATTRIBUTES = "flowfile-attributes";
    public static final String DESTINATION_CONTENT = "flowfile-content";
    public static final PropertyDescriptor PROP_DESTINATION = new PropertyDescriptor.Builder()
            .name("Destination")
            .description("Indicates whether the results of the Vault request are written to the FlowFile content or FlowFile attributes. "
                    + "if using attributes, one attribute is created and written for each key/value pair found in the secret.")
            .required(true)
            .allowableValues(DESTINATION_CONTENT, DESTINATION_ATTRIBUTES)
            .defaultValue(DESTINATION_CONTENT)
            .build();


    // Add the relationships
    public static final Relationship REL_SUCCESS = new Relationship.Builder()
            .name("success")
            .description("If the secret was successfully obtained it will be routed to this relationship")
            .build();

    public static final Relationship REL_FAILURE = new Relationship.Builder()
            .name("failure")
            .description("If unable to communicate with the secret store, the FlowFile will be penalized and routed to this relationship")
            .build();

    private List<PropertyDescriptor> descriptors;

    private Set<Relationship> relationships;


    @Override
    protected void init(final ProcessorInitializationContext context)
    {
        final List<PropertyDescriptor> descriptors = new ArrayList<PropertyDescriptor>();
        descriptors.add(PROP_ROLE_NAME);
        descriptors.add(PROP_INITIAL_TOKEN);
        descriptors.add(PROP_VAULT_SERVER);
        descriptors.add(PROP_SECRET_LOOKUP);
        descriptors.add(PROP_SSL_CONTEXT_SERVICE);
        descriptors.add(PROP_KEYS_TO_FETCH);
        descriptors.add(PROP_DESTINATION);
        this.descriptors = Collections.unmodifiableList(descriptors);

        final Set<Relationship> relationships = new HashSet<Relationship>();
        relationships.add(REL_SUCCESS);
        relationships.add(REL_FAILURE);
        this.relationships = Collections.unmodifiableSet(relationships);
    }

    @Override
    public Set<Relationship> getRelationships()
    {
        return this.relationships;
    }

    @Override
    public final List<PropertyDescriptor> getSupportedPropertyDescriptors()
    {
        return descriptors;
    }

    @OnScheduled
    public void onScheduled(final ProcessContext context)
    {
        // maybe the initial setup of the client should go here
    }

    @Override
    protected Collection<ValidationResult> customValidate(final ValidationContext context)
    {
        final Collection<ValidationResult> results = new ArrayList<>();

        if (context.getProperty(PROP_VAULT_SERVER).evaluateAttributeExpressions().getValue().startsWith("https") && context.getProperty(PROP_SSL_CONTEXT_SERVICE).getValue() == null)
        {
            results.add(new ValidationResult.Builder()
                    .explanation("URL is set to HTTPS protocol but no SSLContext has been specified")
                    .valid(false)
                    .subject("SSL Context")
                    .build());
        }


        return results;
    }


    /**
     * Get a role id for the rolename that is passed.
     *
     * @param roleName The name of the role
     * @param vault    The vault instance from which we should make the request
     */
    private final String getRoleID(String roleName, Vault vault) throws VaultException
    {
        String roleIdLookup = String.format("auth/approle/role/%s/role-id", roleName);
        return vault.logical().read(roleIdLookup).getData().get("role_id");

    }

    /**
     * Get the secret for a particular role
     *
     * @param roleName the name of the role
     * @param vault    a vault instance
     */
    private final String getRoleSecret(String roleName, Vault vault) throws VaultException
    {
        String roleIDLookup = String.format("auth/approle/role/%s/secret-id", roleName);
        return vault.logical().write(roleIDLookup, null)
                .getData().get("secret_id");

    }


    /**
     * Get the client token to use for working with secrets
     *
     * @param context the ProcessContext of the processor
     */
    private String getClientToken(final ProcessContext context, final FlowFile flowFile) throws IOException, VaultException
    {
        ComponentLog logger = getLogger();

        // We go ahead and grab the rolename, as we need it for getting a clientToken.
        final String roleName = context.getProperty(PROP_ROLE_NAME).evaluateAttributeExpressions(flowFile).getValue();

        // Set up the state manager so we can read the state and see what we need.
        // and use that to look up the current state
        StateManager stateManager = context.getStateManager();
        StateMap currentState = stateManager.getState(Scope.CLUSTER);

        // set up our vault object using the initial token, which is why we pass null to the config
        logger.info("Setting up the vault auth");
        Vault vaultAuth = new Vault(getNewVaultConfig(context, flowFile, null));

        // get our roleID, secretID
        String roleID = getRoleID(roleName, vaultAuth);
        String secretID = getRoleSecret(roleName, vaultAuth);


        // Get our clientToken, which we will add to the statemap and also return
        final AuthResponse authResponse = vaultAuth.auth().loginByAppRole("approle", roleID, secretID);
        ClientToken token = new ClientToken(authResponse.getAuthClientToken(), authResponse.getAuthLeaseDuration());


        // We also get the leaseDuration so we can add it all to the logging if needed.
        String clientToken = token.getToken();

        // This gives us lease duration, but token uses is important too
        long leaseDuration = token.getLeaseDuration();


        // We want the issue time and number of uses for the token
        String tokenIssueTime = vaultAuth.logical().read(String.format("auth/token/lookup/%s", clientToken)).getData().get("issue_time");
        String tokenUses = vaultAuth.logical().read(String.format("auth/token/lookup/%s", clientToken)).getData().get("num_uses");

        // turn the time string into a date/time object
        ZonedDateTime theDate = ZonedDateTime.parse(tokenIssueTime, DateTimeFormatter.ISO_OFFSET_DATE_TIME);

        // Get the expiration time for the token
        ZonedDateTime expirationTime =
                ZonedDateTime.parse(vaultAuth.logical().read(String.format("auth/token/lookup/%s", clientToken)).getData().get("expire_time"));


        // Log it out in info, for troubleshooting if needed.
        logger.info("\n"
                + "Updated state with the token \n"
                + "Token Issue Time         : " + theDate + "\n"
                + "Token Lease (s)          : " + leaseDuration + "\n"
                + "Token Expiration Time    : " + expirationTime + "\n"
                + "Total Token Uses Allowed : " + tokenUses);

        // Create a new map, and put the clientToken and expiration in it, then update the Cluster State
        HashMap<String, String> newMap = new HashMap<String, String>();
        newMap.put("clientToken", clientToken);
        newMap.put("tokenExpiration", DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(expirationTime));
        newMap.put("tokenUsesAllowed", tokenUses);
        newMap.put("tokenUsesRemaining", tokenUses);


        if (currentState.getVersion() == -1)
        {
            stateManager.setState(newMap, Scope.CLUSTER);
        } else
        {
            stateManager.replace(currentState, newMap, Scope.CLUSTER);
        }

        // return the client token
        return clientToken;

    }


    /**
     * Get a configuration object for Vault
     *
     * @param context the context of the processor, allows us to get properties
     * @param token   the token we should use to make the request.  It can be null,
     *                in which case we will use the supplied initial token (usually for auth)
     */
    private VaultConfig getNewVaultConfig(final ProcessContext context, final FlowFile flowFile, final String token)
    {

        VaultConfig config = null;
        ComponentLog logger = getLogger();
        try
        {

            int openTimeout = 5;
            int readTimeout = 30;

            /*
              If we are passed a token for lookup, then use it, otherwise use
              the initial token.  Reason being is that the token configured in the
              processor is used for the auth lookup.  All other tokens are client
              tokens used for secret lookup.

              Ternary - if token is not null, the tokenToUse is token, otherwise look it up
            */
            String tokenToUse = (token != null) ? token : context.getProperty(PROP_INITIAL_TOKEN).evaluateAttributeExpressions(flowFile).getValue();

            // Set up the SSL context (if there is one, otherwise null)
            final SSLContextService sslContextService = context.getProperty(PROP_SSL_CONTEXT_SERVICE).asControllerService(SSLContextService.class);
            // set up the config, using SSL if needed.
            if (sslContextService == null)
            {
                config = new VaultConfig()
                        .address(context.getProperty(PROP_VAULT_SERVER).getValue())
                        .token(tokenToUse)
                        .openTimeout(openTimeout)
                        .readTimeout(readTimeout)
                        .build();
            } else
            {
                config = new VaultConfig()
                        .address(context.getProperty(PROP_VAULT_SERVER).getValue())
                        .token(tokenToUse)
                        .openTimeout(openTimeout)
                        .readTimeout(readTimeout)
                        .sslConfig(new SslConfig().trustStoreResource(sslContextService.getTrustStoreFile()).keyStoreResource(sslContextService.getKeyStoreFile(), sslContextService.getKeyStorePassword()))
                        .build();
            }
        }
        catch (VaultException vEx)
        {
            logger.info(String.format("Caught a Vault Error : %s", vEx));

        }

        // and return the Config
        return config;
    }


    @Override
    public void onTrigger(final ProcessContext context, final ProcessSession session) throws ProcessException
    {

        // Set up our Logger
        final ComponentLog logger = getLogger();

        // Get our flowfile
        FlowFile flowFile = session.get();

        // If there is no flowFile, we need to create one
        if (flowFile == null)
        {
            flowFile = session.create();
            logger.info("Needed to create a flowfile.");
        }


        try
        {

            // Set up the state manager so we can read the state and see what we need.
            StateManager stateManager = context.getStateManager();

            // and use that to look up the current state .. and get the token expiration
            StateMap currentState = stateManager.getState(Scope.CLUSTER);

            // If we don't have a token, then we need to get one
            if (currentState.getVersion() == -1 || !currentState.toMap().containsKey("clientToken"))
            {
                // Get client token does return the token if we want it, but it also
                // adds the token to our state, so that we can use it later
                logger.info("need a token for the first time");
                getClientToken(context, flowFile);
                logger.info("we picked up a new token");

                // refresh the currentstate now that we've updated the token
                currentState = stateManager.getState(Scope.CLUSTER);

            }

            // So at this point, we should have a currentState object if we didn't
            // before, so now we can check for expired or maxused token
            String tokenExpiration = currentState.toMap().get("tokenExpiration");

            ZonedDateTime theExpiration = ZonedDateTime.parse(tokenExpiration, DateTimeFormatter.ISO_OFFSET_DATE_TIME);

            // Get the date for now, so we can compare it.
            DateTimeFormatter dtf = DateTimeFormatter.ISO_OFFSET_DATE_TIME;
            String nowString = (OffsetDateTime.now().format(dtf));
            ZonedDateTime nowDate = ZonedDateTime.parse(nowString);

            // Determine if our token is expired or not
            boolean expired;
            if (nowDate.isAfter(theExpiration))
            {
                expired = true;
                //noinspection ConstantConditions
                logger.info("\nEvaluated the expiration (" + theExpiration + ") vs now (" + nowDate + ") and "
                        + "the token is expired.\n"
                        + "expired = " + expired);

            } else
            {
                expired = false;
                //noinspection ConstantConditions
                logger.info("\nEvaluated the expiration (" + theExpiration + ") vs now (" + nowDate + ") and "
                        + "the token is not expired.\n"
                        + "expired = " + expired);
            }

            // Find out current number of uses remaining
            int tokenUsesRemaining = Integer.parseInt(currentState.toMap().get("tokenUsesRemaining"));

            // If expired or we've used all of our token uses, get a new token
            if (expired || tokenUsesRemaining <= 0)
            {
                logger.info("we need a new token");
                getClientToken(context, flowFile);

                // refresh the currentstate now that we've updated the token
                currentState = stateManager.getState(Scope.CLUSTER);

                //tokenUses remaining gets reset
                tokenUsesRemaining = Integer.parseInt(currentState.toMap().get("tokenUsesRemaining"));

            }


            // We have a token for sure now - use that token to make our secret request
            //currentState = stateManager.getState(Scope.CLUSTER)
            String tokenToUse = currentState.toMap().get("clientToken");

            // Set up our Vault object for making requests.
            Vault vaultSecrets = new Vault(getNewVaultConfig(context, flowFile, tokenToUse));

            // Get the secret we are supposed to look up
            final String secret = context.getProperty(PROP_SECRET_LOOKUP).evaluateAttributeExpressions(flowFile).getValue();

            // Look to se if we want the secrets as attributes or content, and we can log it
            String destination = context.getProperty(PROP_DESTINATION).getValue();
            logger.info(String.format("destination for secrets : %s", destination));

          /*
             At this point, regardless of destination, we should check for errors.
             If we get an error, we should route to failure.

             There is no concept of "empty", you can't create a secret without
             any key/value pairs and if the secret doesn't exist you are going
             to get a 403 anyway.
          */

            // Get the keys we are supposed to fetch
            final String keysToFetchValue = context.getProperty(PROP_KEYS_TO_FETCH).getValue();
            final Set<String> keysToFetchMap = keysToFetchValue == null ? new HashSet<>() : new HashSet<>(Arrays.asList(keysToFetchValue.split("\\s*,\\s*")));

            if (DESTINATION_ATTRIBUTES.equals(destination))
            {
                Map<String, String> kvPairs = vaultSecrets.logical().read(secret).getData();
                
                for ( Map.Entry<String, String> key : kvPairs.entrySet())
                {
                    if(keysToFetchMap.isEmpty() || keysToFetchMap.contains(key.getKey()))
                    {
                        flowFile = session.putAttribute(flowFile, key.getKey(), key.getValue());
                    }
                }

                logger.info("Added secrets to attributes.");

            } else if (DESTINATION_CONTENT.equals(destination))
            {
                Map<String, String> kvPairs = vaultSecrets.logical().read(secret).getData();


                // To write the results back out ot flow file
                Map<String, String> newMap = new HashMap<String, String>();
                kvPairs.forEach((key, value) ->
                {
                    if(keysToFetchMap.isEmpty() || keysToFetchMap.contains(key))
                    {
                        newMap.put(key, value);
                    }
                });

                // Take the updated map that has just the keys we want, and turn it into JSON
                Gson gson = new Gson();
                String json = gson.toJson(newMap);
                flowFile = session.write(flowFile, new OutputStreamCallback()
                {

                    @Override
                    public void process(OutputStream out) throws IOException
                    {
                        out.write(json.getBytes());
                    }
                });

                logger.info("Added secrets to content.");

            }

            // subtract the token we just used
            // We need to decrease our token uses remaining and add it back to the Map
            Map<String, String> newMap = new HashMap<String, String>();
            currentState.toMap().forEach((key, value) -> newMap.put(key, value));
            tokenUsesRemaining = tokenUsesRemaining - 1;

            logger.info(String.format("Token Uses Remaining :%d", tokenUsesRemaining));
            newMap.put("tokenUsesRemaining", Integer.toString(tokenUsesRemaining));
            stateManager.replace(currentState, newMap, Scope.CLUSTER);

            // Once we have what we need, make sure to pass it on to the success relationship
            session.transfer(flowFile, REL_SUCCESS);
            session.commit();

            // If we can't connection to Vault, make sure we penalize and follow the failure relationship.
        }
        catch (final VaultException vEx)
        {

            flowFile = session.penalize(flowFile);
            if (!vEx.getMessage().isEmpty())
            {

                /*
                flowFile = session.write(flowFile, new OutputStreamCallback() {

                    @Override
                    public void process(OutputStream out) throws IOException {
                        out.write(vEx.getMessage().getBytes());
                    }
                });
                */

                // the lamba way, but how to do an override?
                flowFile = session.write(flowFile, (OutputStream out) -> out.write(vEx.toString().getBytes()));
                logger.error("Unable to communicate with Vault when processing "
                        + flowFile + " due to " + vEx);
            } else
            {
                String message = "Unable to communicate with Vault when processing " + flowFile;


                flowFile = session.write(flowFile, new OutputStreamCallback()
                {

                    @Override
                    public void process(OutputStream out) throws IOException
                    {
                        out.write(message.getBytes());
                    }
                });
                logger.error(message);
            }
            session.transfer(flowFile, REL_FAILURE);
            session.commit();

        }
        catch (IOException ioEx)
        {
            flowFile = session.penalize(flowFile);
            logger.error("We caught an IO Exception "
                    + flowFile + " due to " + ioEx);
            session.transfer(flowFile, REL_FAILURE);
            session.commit();
        }
    }
}
