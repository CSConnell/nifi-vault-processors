package com.staq.nifi.processors.vaultprocessors.util;

public class ClientToken
{
    public ClientToken(String token, long leaseDuration)
    {
        this.token = token;
        this.leaseDuration = leaseDuration;
    }

    public String getToken()
    {
        return token;
    }

    public long getLeaseDuration()
    {
        return leaseDuration;
    }

    private final String token;
    private final long leaseDuration;
}
